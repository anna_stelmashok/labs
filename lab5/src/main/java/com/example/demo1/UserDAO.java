package com.example.demo1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type User dao.
 */
public class UserDAO {
    /**
     * Find by name user account.
     *
     * @param name the name
     * @return the user account
     */
    public UserAccount findByName(String name) {
        Connection connection = ConnectionManager.getConnection();
        UserAccount user = null;
        try {
            ConnectionManager.getConnection();
            PreparedStatement prepStatement
                    = connection.prepareStatement("SELECT DISTINCT name, password, gender FROM user WHERE name = ? ");
            prepStatement.setString(1, name);
            ResultSet resObj = prepStatement.executeQuery();
            while (resObj.next()) {
                user = new UserAccount();
                String userName = resObj.getString("name");
                user.setUserName(userName);
                String password = resObj.getString("password");
                user.setPassword(password);
                String gender = resObj.getString("gender");
                user.setGender(gender);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return user;
    }

    /**
     * Find user user account.
     *
     * @param userName the user name
     * @param password the password
     * @return the user account
     */
    public UserAccount findUser(String userName, String password) {
        UserAccount u = findByName(userName);
        if (u != null && u.getPassword().equals(password)) {
            return u;
        }
        return null;
    }
}