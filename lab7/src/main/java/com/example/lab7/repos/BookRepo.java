package com.example.lab7.repos;

import com.example.lab7.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * The interface Book repo.
 */
public interface BookRepo extends JpaRepository<Book, Long> {
    List<Book> findAll();
}