package com.example.lab7.service;

import com.example.lab7.domain.Book;
import com.example.lab7.repos.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The type Book service.
 */
@Service
public class BookService {
    @Autowired
    private BookRepo bookRepo;

    /**
     * Find all list.
     *
     * @return the list
     */
    public List<Book> findAll() {
        return bookRepo.findAll();
    }


}
