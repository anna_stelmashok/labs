package com.example.lab7.controller;

import com.example.lab7.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;


/**
 * The type Main controller.
 */
@Controller
public class MainController {
    @Autowired
    private BookService bookService;

    /**
     * Main string.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/")
    public String main(Map<String, Object> model) {
        model.put("title", "title");
        model.put("bookList", bookService.findAll());
        return "main";
    }

}
