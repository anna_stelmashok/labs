package com.example.demo1;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Data dao.
 */
//класс для работы с БД, в данном классе представлена в виде хэш мапе
public class DataDAO {

   //создание базы данных
   private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();

   static {
      initUsers();
   }

   //инициализация пользователей и добавление данных о новых пользователях в БД
   private static void initUsers() {
      UserAccount emp = new UserAccount("employee1", "123", UserAccount.GENDER_MALE, //
            SecurityConfig.ROLE_EMPLOYEE);
      UserAccount mng = new UserAccount("manager1", "123", UserAccount.GENDER_MALE, //
            SecurityConfig.ROLE_EMPLOYEE, SecurityConfig.ROLE_MANAGER);
      mapUsers.put(emp.getUserName(), emp);
      mapUsers.put(mng.getUserName(), mng);
   }

   /**
    * Find user user account.
    *
    * @param userName the user name
    * @param password the password
    * @return the user account
    */
   //проверка введенных пользователем данных
   public static UserAccount findUser(String userName, String password) {
      UserAccount u = mapUsers.get(userName);
      if (u != null && u.getPassword().equals(password)) {
         return u;
      }
      return null;
   }

}