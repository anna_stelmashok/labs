package com.example.demo1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The type Security config (класс режимов доступа)
 */
public class SecurityConfig {

	//константа для роли директора(менеджера)
	public static final String ROLE_MANAGER = "MANAGER";
	//константа для роли работника
	public static final String ROLE_EMPLOYEE = "EMPLOYEE";

	//хэш-мап для конфигурации ролей
	private static final Map<String, List<String>> mapConfig = new HashMap<String, List<String>>();

	static {
		init();
	}

	private static void init() {
		//список доступных url для работников
		List<String> urlPatterns1 = new ArrayList<String>();

		urlPatterns1.add("/userInfo");
		urlPatterns1.add("/employeeTask");

		mapConfig.put(ROLE_EMPLOYEE, urlPatterns1);

		List<String> urlPatterns2 = new ArrayList<String>();

		//список доступных url для менеджеров
		urlPatterns2.add("/userInfo");
		urlPatterns2.add("/managerTask");

		mapConfig.put(ROLE_MANAGER, urlPatterns2);
	}

	/**
	 * Gets all app roles.
	 *
	 * @return the all app roles
	 */
	public static Set<String> getAllAppRoles() {
		return mapConfig.keySet();
	}

	/**
	 * Gets url patterns for role.
	 *
	 * @param role the role
	 * @return the url patterns for role
	 */
	public static List<String> getUrlPatternsForRole(String role) {
		return mapConfig.get(role);
	}

}