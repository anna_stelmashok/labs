package com.example.demo1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// класс для авторизации пользователей
public class Login extends HttpServlet {

    //вызывается метод init() в servlet, чтобы инициализировать его
    public void init() {
    }

    //метод, который считывает данные, введеные пользователем, данные заносятся в соответсвующие переменные
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // переменная для имя пользователя
        String userName = request.getParameter("userName");
        //переменная для пароля
        String password = request.getParameter("password");
        //создается объект класса, далее вызывается метод, который проверяет правильно ли пользователь ввел данные и есть ли они в базе
        UserAccount userAccount = DataDAO.findUser(userName, password);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (userAccount == null) {
            //если пользователь не найден, то выводится html-код с сообщением об ошибке
            String errorMessage = "Invalid userName or Password";
            out.println("<html><body>");
            out.println("<h1>" + errorMessage + "</h1>");
            out.println("</body></html>");
            return;
        }
        //если пользователь найден - выводится html-код с сообщением об успешном входе
        out.println("<html><body>");
        out.println("<h1>" + "Ok" + "</h1>");
        out.println("</body></html>");
    }

    //обработка запроса (получение данных)
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    }

    ////метод destroy() используется для разрушения servlet
    public void destroy() {
    }
}