package com.example.demo1;

import java.util.ArrayList;
import java.util.List;

//класс для описания ползователей
public class UserAccount {
   //константа для мужского пола
   public static final String GENDER_MALE = "M";
   //константа ля женского пола
   public static final String GENDER_FEMALE = "F";

   //имя пользователя
   private String userName;
   //пол
   private String gender;
   //пароль
   private String password;

   //список ролей
   private List<String> roles;

   //конструктор
   public UserAccount() {

   }

   //Конструктор создания ролей
      public UserAccount(String userName, String password, String gender, String... roles) {
      //имя пользователя
      this.userName = userName;
      //пароль
      this.password = password;
      //пол
      this.gender = gender;

      //добавление роли
      this.roles = new ArrayList<String>();
      if (roles != null) {
         for (String r : roles) {
            this.roles.add(r);
         }
      }
   }

   /**
    * Gets user name.
    *
    * @return the user name
    */
   public String getUserName() {
      return userName;
   }

   /**
    * Sets user name.
    *
    * @param userName the user name
    */
   public void setUserName(String userName) {
      this.userName = userName;
   }

   /**
    * Gets gender.
    *
    * @return the gender
    */
   public String getGender() {
      return gender;
   }

   /**
    * Sets gender.
    *
    * @param gender the gender
    */
   public void setGender(String gender) {
      this.gender = gender;
   }

   /**
    * Gets password.
    *
    * @return the password
    */
   public String getPassword() {
      return password;
   }

   /**
    * Sets password.
    *
    * @param password the password
    */
   public void setPassword(String password) {
      this.password = password;
   }

   /**
    * Gets roles.
    *
    * @return the roles
    */
   public List<String> getRoles() {
      return roles;
   }

   /**
    * Sets roles.
    *
    * @param roles the roles
    */
   public void setRoles(List<String> roles) {
      this.roles = roles;
   }
}