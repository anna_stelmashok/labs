package com.example.demo1;

import java.io.*;
import javax.servlet.http.*;

/**
 * The type Hello servlet.
 */
public class HelloServlet extends HttpServlet {
    private String message;

    //вызывается метод init() в servlet, чтобы инициализировать его
    public void init() {
        message = "Hello Servlet!";
    }

    //метод для обработки GET-запросов
    //параметр типа HttpServletRequest инкапсулирует всю информацию о запросе
    //параметр типа HttpServletResponse позволяет управлять ответом
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //с помощью вызова response.setContentType("text/html") устанавливается тип ответа (в данном случае, мы говорим, что ответ представляет код html)
        response.setContentType("text/html");

        //с помощью метода getWriter() объекта мы можем получить объект PrintWriter, через который можно отправить какой-то определенный ответ пользователю
        PrintWriter out = response.getWriter();
        //через метод println() пользователю отправляется простейший html-код
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }

    //метод destroy() используется для разрушения servlet
    public void destroy() {
    }
}